from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from bs4 import BeautifulSoup as bs
import pandas as pd
import json
import time
import re

def get_chrome_web_driver(options):
    return webdriver.Chrome("./chromedriver", chrome_options=options)

def get_web_driver_options():
    return webdriver.ChromeOptions()

def set_ignore_certificate_error(options):
    options.add_argument('--ignore-certificate-errors')

def set_browser_as_incognito(options):
    options.add_argument('--incognito')

def set_automation_as_head_less(options):
    options.add_argument('--headless')



# setting up the webdriver and configurations
options = get_web_driver_options()
set_ignore_certificate_error(options)
set_browser_as_incognito(options)
set_automation_as_head_less(options)
driver = get_chrome_web_driver(options)


    
def scrape_dealers(dealer_data: list):
    for dealer in dealer_data:
        next_dealer=dealer['URL']
        print(next_dealer)
        driver.get(next_dealer)
        time.sleep(3)
        page_source=driver.page_source
        soup=bs(page_source,'lxml')
        number_ofListings=soup.find('a',attrs={'data-linkname':'dealer-inventory-all'})
        number_ofListings=number_ofListings.text if number_ofListings else '0'
        number_ofListingsClean= [int(s) for s in re.findall('\\d+', number_ofListings)]
        numListings=number_ofListingsClean[0]
        dealer['numberOfListings']=str(numListings)
        time.sleep(1)
    return dealer_data

#read in file with dealer info
with open('output_data_all.json', "r") as f:
        data = json.loads(f.read())  
        
dealer_info_full=scrape_dealers(data)

#write new file with number of listings for each dealer
with open('output_data_all_dealers.json','w') as f:
    json.dump(data,f,indent=4)