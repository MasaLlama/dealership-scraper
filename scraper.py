from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from bs4 import BeautifulSoup as bs
import pandas as pd
import json
import time

def get_chrome_web_driver(options):
    return webdriver.Chrome("./chromedriver", chrome_options=options)

def get_web_driver_options():
    return webdriver.ChromeOptions()

def set_ignore_certificate_error(options):
    options.add_argument('--ignore-certificate-errors')

def set_browser_as_incognito(options):
    options.add_argument('--incognito')

def set_automation_as_head_less(options):
    options.add_argument('--headless')



# setting up the webdriver and configurations
options = get_web_driver_options()
set_ignore_certificate_error(options)
set_browser_as_incognito(options)
set_automation_as_head_less(options)
driver = get_chrome_web_driver(options)

#list of all states : https://www.cars.com/sitemap/



#within each state,
#list of all cities : example AL : https://www.cars.com/sitemap/shopping/al/
#the following is a list of dealers in each city 
#get all links under ul tag, class_='sds-list top-links'

#then, list of all dealers under ul tag, class_='sds-list top-links'

#get number of listings from link text in following tag.


def link_builder(state: str,**kwargs: str)->str:
    BASE_URL='https://www.cars.com/sitemap/shopping/'
    state=state.lower()
    if kwargs.get('city'):
        #return formatted url with city and state name
        return BASE_URL+(f'dealers/{kwargs["city"]}-{state}/')
    else:
        #valid url with only state name
        return BASE_URL+(f'{state}/')


states_links=driver.find_elements(By.XPATH,'/html/body/section/section[3]/div[2]/div/ul/li')
states_list= [state.text for state in states_links]
states_list
listofdealers=[]
Base_Url='https://www.cars.com'
driver.get('https://www.cars.com/sitemap/')
#scrape_info={'name':next_state,'city':next_city,'dealers':}
#dealer_info={'name':next_dealer,'URL':''}
#dealer_info={'state':'','city':'','name':next_dealer,'URL':''}
for state in states_list:
    next_state=state
    #get links for all dealers
    driver.get(link_builder(next_state))
    #get all links
    #find all cities in state -> building list of links 
    cities_links=driver.find_elements(By.XPATH,'/html/body/section/section[2]/div[2]/div/ul/li')
    cities_list=[city.text.split()[0].lower() for city in cities_links]
    for city in cities_list:
        next_city=city
        #get url for every dealer
        #cityDict={'name':next_city,'dealers':[]}
        #navigate to city page
        driver.get(link_builder(next_state,city=next_city))
        time.sleep(2)
        #dealers_links=driver.find_elements(By.XPATH,'/html/body/section/section/div[2]/div/ul/li')
        #dealers_list=[dealer.text for dealer in dealers_links]
        page_source=driver.page_source
        soup=bs(page_source,'lxml')
        #create list of all dealers in that city 
        dealers=soup.find_all('a',attrs={'data-linkname':'dpp'})
        for dealer in dealers:

            link=Base_Url + (dealer.attrs['href'])
            dealer_info={'state':next_state,'city':next_city,'name':dealer.text,'URL':link}
            listofdealers.append(dealer_info)
        driver.back()
        time.sleep(1)
    #all cities are done
    #go back to states page
    driver.back()
    time.sleep(1)
    
    

with open('output_data_all.json','w') as f:
    json.dump(listofdealers,f,indent=4)
    
with open('output_data_all.json', "r") as f:
        data = json.loads(f.read())  
    
for dealer in data:
    next_dealer=dealer['URL']
    print(next_dealer)
    driver.get(next_dealer)
    time.sleep(3)
    page_source=driver.page_source
    soup=bs(page_source,'lxml')
    number_ofListings=soup.find('a',attrs={'data-linkname':'dealer-inventory-all'})
    number_ofListings=number_ofListings.text if number_ofListings else '0'
    number_ofListingsClean= [int(s) for s in re.findall('\\d+', number_ofListings)]
    numListings=number_ofListingsClean[0]
    dealer['numberOfListings']=str(numListings)
    time.sleep(1)
print(data)

with open('output_data_all_dealers.json','w') as f:
    json.dump(data,f,indent=4)